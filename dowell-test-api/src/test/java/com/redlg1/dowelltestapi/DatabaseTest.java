package com.redlg1.dowelltestapi;

import com.redlg1.dowelltestapi.model.entity.CommentEntity;
import com.redlg1.dowelltestapi.model.repository.CommentRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Slf4j
@SpringBootTest
public class DatabaseTest {
    @Autowired
    private CommentRepository repository;

    @Test
    public void insertComment(){
        var comment = CommentEntity.builder()
                .text("123")
                .time(Timestamp.valueOf(LocalDateTime.now()))
                .notificationEntity(null)
                .build();
        comment = repository.save(comment);
        log.debug(String.valueOf(comment.getId()));
    }
}
