package com.redlg1.dowelltestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DowellTestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DowellTestApiApplication.class, args);
	}

}
