package com.redlg1.dowelltestapi.service;

import com.redlg1.dowelltestapi.logic.BusinessLogic;
import com.redlg1.dowelltestapi.model.entity.CommentEntity;
import com.redlg1.dowelltestapi.model.repository.CommentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Slf4j
@Service
public class CommentService {
    @Autowired
    private CommentRepository repository;
    @Autowired
    private NotificationService notificationService;

    public Page<CommentEntity> getAllComments(Pageable pageable){
        return repository.findAll(pageable);
    }

    public void addComment(String text){
        new Thread(() -> {
            var time = new Timestamp(System.currentTimeMillis());
            var entity = CommentEntity.builder()
                    .text(text)
                    .time(time)
                    .build();
            try{
                BusinessLogic.doSomeWorkOnCommentCreation();
                notificationService.addNotification(entity);
            } catch (Exception e){
                log.error(e.getMessage());
                repository.delete(entity);
            }
        }).start();
    }
}
