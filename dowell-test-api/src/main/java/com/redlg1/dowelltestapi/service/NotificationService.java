package com.redlg1.dowelltestapi.service;

import com.redlg1.dowelltestapi.logic.BusinessLogic;
import com.redlg1.dowelltestapi.model.entity.CommentEntity;
import com.redlg1.dowelltestapi.model.entity.NotificationEntity;
import com.redlg1.dowelltestapi.model.repository.NotificationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Slf4j
@Service
public class NotificationService {
    @Autowired
    private NotificationRepository repository;

    public Page<NotificationEntity> getAllNotifications(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public void addNotification(CommentEntity commentEntity){
        new Thread(() -> {
            var entity = NotificationEntity.builder()
                    .id(null)
                    .commentEntity(commentEntity)
                    .time(Timestamp.valueOf(LocalDateTime.now()))
                    .delivered(null)
                    .build();
            entity = repository.save(entity);
            try {
                BusinessLogic.doSomeWorkOnNotification();
                entity.setTime(Timestamp.valueOf(LocalDateTime.now()));
                entity.setDelivered(true);
                repository.save(entity);
            } catch (RuntimeException e) {
                log.error(e.getMessage());
                entity.setTime(Timestamp.valueOf(LocalDateTime.now()));
                entity.setDelivered(false);
                repository.save(entity);
            }
        }).start();
    }
}
