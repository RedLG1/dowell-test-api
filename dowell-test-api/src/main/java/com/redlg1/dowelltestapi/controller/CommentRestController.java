package com.redlg1.dowelltestapi.controller;

import com.redlg1.dowelltestapi.dto.CommentDto;
import com.redlg1.dowelltestapi.dto.PostRequestDto;
import com.redlg1.dowelltestapi.model.entity.CommentEntity;
import com.redlg1.dowelltestapi.service.CommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Slf4j
@RestController
public class CommentRestController {
    @Autowired
    private CommentService service;

    @PostMapping("/rest/comment")
    public void addComment(@RequestBody @Valid PostRequestDto body){
        log.debug(body.toString());
        String text = body.getText();
        service.addComment(text);
    }

    @GetMapping(value = {"/rest/comments/{page}", "/rest/comments/{page}/{size}"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CommentDto> getAll(@PathVariable int page, @PathVariable(required = false) Integer size){
        var result = service.getAllComments(
                PageRequest.of(page - 1, size != null ? size : 10)
        );
        List<CommentDto> entities = new LinkedList<>();
        for(CommentEntity entity : result){
            var dto = CommentDto.builder()
                    .id(entity.getId())
                    .text(entity.getText())
                    .time(entity.getTime())
                    .build();
            entities.add(dto);
        }
        return entities;
    }

}
