package com.redlg1.dowelltestapi.controller;

import com.redlg1.dowelltestapi.model.entity.NotificationEntity;
import com.redlg1.dowelltestapi.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Slf4j
@RestController
public class NotificationRestController {
    @Autowired
    private NotificationService service;

    @GetMapping(value = {"/rest/notifications/{page}", "/rest/notifications/{page}/{size}"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NotificationEntity> getAll(@PathVariable int page, @PathVariable(required = false) Integer size){
        var result = service.getAllNotifications(
                PageRequest.of(page - 1, size != null ? size : 10)
        );
        return result.getContent();
    }
}
