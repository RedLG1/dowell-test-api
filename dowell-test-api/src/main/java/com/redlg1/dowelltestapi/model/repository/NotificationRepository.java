package com.redlg1.dowelltestapi.model.repository;

import com.redlg1.dowelltestapi.model.entity.NotificationEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Repository
public interface NotificationRepository extends PagingAndSortingRepository<NotificationEntity, Long> {

}
