package com.redlg1.dowelltestapi.model.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Entity
@Table(name = "comments")
@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CommentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String text;
    private Timestamp time;
    @OneToMany(mappedBy = "commentEntity", cascade = CascadeType.ALL)
    private List<NotificationEntity> notificationEntity;
}
