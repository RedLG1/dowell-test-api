package com.redlg1.dowelltestapi.model.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Entity
@Table(name = "notifications")
@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class NotificationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comment_id", referencedColumnName = "id")
    private CommentEntity commentEntity;
    private Timestamp time;
    private Boolean delivered;
}
