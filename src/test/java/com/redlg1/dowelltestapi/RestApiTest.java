package com.redlg1.dowelltestapi;

import com.redlg1.dowelltestapi.dto.CommentDto;
import com.redlg1.dowelltestapi.dto.PostRequestDto;
import com.redlg1.dowelltestapi.model.RestResponsePage;
import com.redlg1.dowelltestapi.model.entity.NotificationEntity;
import com.redlg1.dowelltestapi.model.repository.CommentRepository;
import com.redlg1.dowelltestapi.model.repository.NotificationRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootTest(classes = DowellTestApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Slf4j
public class RestApiTest {
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private CommentRepository commentRepository;

    @Test
    public void insertAndCheckData() throws InterruptedException {
        ExecutorService executor = Executors.newScheduledThreadPool(50);
        AtomicInteger commentCount = new AtomicInteger();
        AtomicInteger notificationCount = new AtomicInteger();
        List<Future<?>> list = new LinkedList<>();
        for (int i = 0; i < 1000; ++i) {
            Runnable runnable = () -> {
                try {
                    var urlPath = "http://localhost:8080/rest/v1/comments";
                    var text = UUID.randomUUID().toString();
                    var comment = new PostRequestDto(text);
                    var template = new RestTemplate();
                    var postRequest = getRequest(comment, HttpMethod.POST, urlPath);
                    var responseEntity = template.exchange(postRequest, String.class);
                    var urlSuffix = "/1/1000";
                    if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
                        Thread.sleep(3000);
                        ParameterizedTypeReference<RestResponsePage<CommentDto>> responseType = new ParameterizedTypeReference<>() {};
                        var request = getRequest(null, HttpMethod.GET, urlPath + urlSuffix);
                        template = new RestTemplate();
                        ResponseEntity<RestResponsePage<CommentDto>> commentResponse = template.exchange(request, responseType);
                        if (commentResponse.getBody() != null) {
                            for (CommentDto commentDto : commentResponse.getBody()) {
                                if (commentDto.getText().contains(text)) {
                                    commentCount.getAndIncrement();
                                    ParameterizedTypeReference<RestResponsePage<NotificationEntity>> notificationType = new ParameterizedTypeReference<>() {};
                                    RestTemplate notificationTemplate = new RestTemplate();
                                    urlPath = "http://localhost:8080/rest/v1/notifications";
                                    request = getRequest(null, HttpMethod.GET, urlPath + urlSuffix);
                                    var response = notificationTemplate.exchange(request, notificationType);
                                    if(response.getBody() != null && !response.getBody().getContent().isEmpty()){
                                        List<NotificationEntity> notificationEntities = response.getBody().getContent();
                                        for(NotificationEntity entity : notificationEntities){
                                            var dto = entity.getDto();
                                            if(dto.getCommentId().equals(commentDto.getId())){
                                                Boolean delivered = dto.getDelivered();
                                                if(delivered != null && delivered){
                                                    notificationCount.getAndIncrement();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (URISyntaxException | InterruptedException e) {
                    log.error(e.getMessage());
                }
            };
            var future = executor.submit(runnable);
            list.add(future);
        }
        for(Future<?> future : list){
            try {
                future.get(30, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                future.cancel(true);
                log.error(e.getMessage());
            }
        }
        BigDecimal commentResultValue = BigDecimal.valueOf(commentCount.get() * 0.1).setScale(2,RoundingMode.HALF_UP);
        BigDecimal notificationResultValue = BigDecimal.valueOf(notificationCount.get() * 0.1).setScale(2,RoundingMode.HALF_UP);
        log.info("------------------------------------------------");
        log.info("RESULT");
        log.info("------------------------------------------------");
        log.info("Comment sent - {}%", commentResultValue);
        log.info("Notification sent - {}%", notificationResultValue);
        log.info("-------------------------------------------------");
        Thread.sleep(1000);
        Assertions.assertEquals(commentRepository.count(), commentCount.get());
        Assertions.assertEquals(notificationRepository.countByDeliveredIsTrue(), notificationCount.get());
        executor.shutdown();
    }

    private RequestEntity<?> getRequest(Object object, HttpMethod method, String url) throws URISyntaxException {
        var requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new RequestEntity<>(
                object,
                requestHeaders,
                method,
                new URI(url)
        );
    }
}
