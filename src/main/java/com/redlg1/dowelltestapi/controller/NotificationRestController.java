package com.redlg1.dowelltestapi.controller;

import com.redlg1.dowelltestapi.model.entity.NotificationEntity;
import com.redlg1.dowelltestapi.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Slf4j
@RestController
public class NotificationRestController {
    @Autowired
    private NotificationService service;

    @GetMapping(value = {"/rest/v1/notifications/{page}", "/rest/v1/notifications/{page}/{size}"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<NotificationEntity> getAll(@PathVariable int page, @PathVariable(required = false) Integer size){
        return service.getAllNotifications(
                PageRequest.of(page - 1, size != null && size > 0 ? size : 10, Sort.by("id"))
        );
    }
}
