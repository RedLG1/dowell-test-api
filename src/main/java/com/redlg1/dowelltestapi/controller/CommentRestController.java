package com.redlg1.dowelltestapi.controller;

import com.redlg1.dowelltestapi.dto.PostRequestDto;
import com.redlg1.dowelltestapi.exception.RestApiException;
import com.redlg1.dowelltestapi.model.entity.CommentEntity;
import com.redlg1.dowelltestapi.service.CommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Slf4j
@RestController
public class CommentRestController {
    @Autowired
    private CommentService service;

    @PostMapping("/rest/v1/comments")
    public void addComment(@RequestBody @Valid PostRequestDto body) throws RestApiException {
        log.debug(body.toString());
        String text = body.getText();
        service.addComment(text);
    }

    @GetMapping(value = {"/rest/v1/comments/{page}", "/rest/v1/comments/{page}/{size}"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<CommentEntity> getAll(@PathVariable int page, @PathVariable(required = false) Integer size){
        return service.getAllComments(
                PageRequest.of(page - 1, size != null && size > 0 ? size : 10, Sort.by("id"))
        );
    }

}

