package com.redlg1.dowelltestapi.logic;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Slf4j
public class BusinessLogic {
    public static void sleepAndRandomThrowRuntimeException(int seconds, int exceptionProbabilityProc){
        try {
            var sleepValue = seconds * 1000 * Math.random();
            log.trace(String.valueOf(sleepValue));
            Thread.sleep((long) sleepValue);
        } catch (InterruptedException e) {}
        int randomProc = (int) (100 * Math.random());
        if(exceptionProbabilityProc > randomProc) throw new RuntimeException();
    }
    public static void doSomeWorkOnNotification(){
        sleepAndRandomThrowRuntimeException(2, 10);
    }
    public static void doSomeWorkOnCommentCreation(){
        sleepAndRandomThrowRuntimeException(1, 30);
    }
}