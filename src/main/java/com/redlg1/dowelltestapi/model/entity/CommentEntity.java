package com.redlg1.dowelltestapi.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Entity
@Table(name = "comments")
@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CommentEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String text;
    private Timestamp time;
    @JsonBackReference
    @OneToOne(mappedBy = "commentEntity", fetch = FetchType.LAZY)
    private NotificationEntity notificationEntity;
}
