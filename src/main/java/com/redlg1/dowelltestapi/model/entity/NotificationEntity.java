package com.redlg1.dowelltestapi.model.entity;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.redlg1.dowelltestapi.dto.NotificationDto;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Entity
@Table(name = "notifications",indexes = { @Index(name = "idx_notification_comment_id", columnList = "comment_id") })
@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class NotificationEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JsonProperty("commentEntity")
    @JsonIdentityReference(alwaysAsId=true)
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "comment_id", referencedColumnName = "id")
    private CommentEntity commentEntity;
    private Timestamp time;
    private Boolean delivered;

    public NotificationDto getDto(){
        return NotificationDto.builder()
                .id(this.id)
                .time(this.time)
                .commentId(this.commentEntity.getId())
                .delivered(this.delivered)
                .build();
    }
}
