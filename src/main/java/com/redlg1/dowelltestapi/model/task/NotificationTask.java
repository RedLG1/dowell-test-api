package com.redlg1.dowelltestapi.model.task;

import com.redlg1.dowelltestapi.logic.BusinessLogic;
import com.redlg1.dowelltestapi.model.entity.NotificationEntity;
import com.redlg1.dowelltestapi.model.repository.NotificationRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Slf4j
public class NotificationTask extends Thread{

    private NotificationEntity entity;
    @Autowired
    private NotificationRepository notificationRepository;

    @Override
    public void run() {
        if(entity != null && notificationRepository != null){
            try {
                BusinessLogic.doSomeWorkOnNotification();
                entity.setDelivered(true);
            } catch (Exception e) {
                log.error(e.getMessage());
                entity.setDelivered(false);
            }
            notificationRepository.save(entity);
        }
    }
}
