package com.redlg1.dowelltestapi.service;

import com.redlg1.dowelltestapi.model.task.NotificationTask;
import com.redlg1.dowelltestapi.model.entity.CommentEntity;
import com.redlg1.dowelltestapi.model.entity.NotificationEntity;
import com.redlg1.dowelltestapi.model.repository.NotificationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@Slf4j
@Service
public class NotificationService {
    @Autowired
    private NotificationRepository repository;
    private ExecutorService executor = Executors.newFixedThreadPool(100);

    public Page<NotificationEntity> getAllNotifications(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public void addNotification(final CommentEntity commentEntity){
        Runnable runnable = ()->{
            NotificationEntity notificationEntity = NotificationEntity.builder()
                    .id(null)
                    .commentEntity(commentEntity)
                    .time(Timestamp.valueOf(LocalDateTime.now()))
                    .delivered(null)
                    .build();
            notificationEntity = repository.save(notificationEntity);
            var task = new NotificationTask();
            task.setEntity(notificationEntity);
            task.setNotificationRepository(repository);
            task.start();
        };
        runnable.run();
    };
}
