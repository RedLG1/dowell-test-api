package com.redlg1.dowelltestapi.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = {RestApiException.class})
    private ResponseEntity<Object> handle(RestApiException e){
        var exception = new ApiExceptionEntity(
                e.getLocalizedMessage(),
                e.getStatus(),
                ZonedDateTime.now()
        );
        return new ResponseEntity<>(exception, e.getStatus());
    }


    @RequiredArgsConstructor
    @Setter
    @Getter
    @ToString
    public static class ApiExceptionEntity{
        private final String message;
        private final HttpStatus status;
        private final ZonedDateTime time;
    }
}
