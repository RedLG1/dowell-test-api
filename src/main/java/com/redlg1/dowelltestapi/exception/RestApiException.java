package com.redlg1.dowelltestapi.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 22-01-2021
 */

@Getter
public class RestApiException  extends Exception{

    private HttpStatus status;

    public RestApiException(String message) {
        super(message);
        this.status = HttpStatus.BAD_REQUEST;
    }

    public RestApiException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public RestApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
