package com.redlg1.dowelltestapi.dto;

import lombok.*;

import java.sql.Timestamp;

@EqualsAndHashCode
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CommentDto {
    private Long id;
    private String text;
    private Timestamp time;
}
