package com.redlg1.dowelltestapi.dto;

import lombok.*;

import java.sql.Timestamp;

@EqualsAndHashCode
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class NotificationDto {
    private Long id;
    private Long commentId;
    private Timestamp time;
    private Boolean delivered;
}
