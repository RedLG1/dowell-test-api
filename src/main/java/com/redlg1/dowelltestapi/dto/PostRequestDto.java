package com.redlg1.dowelltestapi.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Created by Yaroslav Kravchenko {@literal yaroslav.kravchenko.work@gmail.com}
 * @version 1.0
 * @since 21-01-2021
 */

@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class PostRequestDto {
    @NotNull(message = "Add field \"text\" or insert data into your comment")
    @Size(min = 1, max = 255, message = "Invalid text length!")
    private String text;
}
