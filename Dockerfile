FROM adoptopenjdk/openjdk11:centos-nightly-slim

ADD ./target/dowell-test-api-0.0.1-SNAPSHOT.jar /usr/share/app/app.jar
RUN chmod 777 /usr/share/app/app.jar
ENTRYPOINT ["java", "-jar", "/usr/share/app/app.jar"]
