# dowell-test-api

---

### Инструкция по запуску
Из корневой папки проекта выполнить команду на выбор:

* __sudo docker-compose -f docker-compose.yml up__

* __docker-compose up__

---

### API позволяет:
1. Добавить комментарий
2. Получить список добавленных комментариев с пагинацией
3. Получить список созданных уведомлений с пагинацией

---

### 1. Добавление комментария
  
Запрос:
```
 POST http://localhost:8080/rest/v1/comments
```
Тело запроса:
```json
{
  "text": "Тестовый комментарий"
}
```
#### Тело запроса не должно превышать varchar(255), иначе комментарий не добавится и вернет тело ответа:
```json
{
  "timestamp": "2021-01-24T21:08:42.418+00:00",
  "status": 400,
  "error": "Bad Request",
  "message": "",
  "path": "/rest/v1/comments"
}
```

---

### 2. Получение списка добавленных комментариев с пагинацией

#### Информацию можно получить после ввода номера страницы(1-N, обязательно), количество элементов(1-N, стандартное значение - 10).

Запрос:
```
 GET http://localhost:8080/rest/v1/comments/1
```
Тело ответа:
```json
{
  "content": [
    {
      "id": 1,
      "text": "eryjetd 7",
      "time": "2021-01-24T20:33:49.862+00:00"
    },
    {
      "id": 2,
      "text": "eryjetd 7",
      "time": "2021-01-24T20:33:52.162+00:00"
    },
    {
      "id": 3,
      "text": "eryjetd 7",
      "time": "2021-01-24T20:33:53.492+00:00"
    },
    {
      "id": 4,
      "text": "eryjetd 7",
      "time": "2021-01-24T20:33:55.040+00:00"
    },
    {
      "id": 5,
      "text": "eryjetd 7eryjetd 7eryjetd 7eryjetd",
      "time": "2021-01-24T20:51:37.884+00:00"
    },
    {
      "id": 6,
      "text": "Тестовый комментарий",
      "time": "2021-01-24T21:01:29.053+00:00"
    },
    {
      "id": 7,
      "text": "Тестовый комментарий...5",
      "time": "2021-01-24T21:38:35.928+00:00"
    },
    {
      "id": 9,
      "text": "Тестовый комментарий...",
      "time": "2021-01-24T21:38:45.898+00:00"
    },
    {
      "id": 10,
      "text": "Тестовый",
      "time": "2021-01-24T21:38:57.587+00:00"
    },
    {
      "id": 12,
      "text": "Тест",
      "time": "2021-01-24T21:39:04.509+00:00"
    }
  ],
  "pageable": {
    "sort": {
      "sorted": true,
      "unsorted": false,
      "empty": false
    },
    "pageNumber": 0,
    "pageSize": 10,
    "offset": 0,
    "paged": true,
    "unpaged": false
  },
  "last": true,
  "totalPages": 1,
  "totalElements": 10,
  "sort": {
    "sorted": true,
    "unsorted": false,
    "empty": false
  },
  "first": true,
  "number": 0,
  "numberOfElements": 10,
  "size": 10,
  "empty": false
}
```

---

### 3. Получение списка добавленных уведомлений с пагинацией

#### Информацию можно получить после ввода номера страницы(1-N, обязательно), количество элементов(1-N, стандартное значение - 10).

Запрос:
```
 GET http://localhost:8080/rest/v1/notifications/1/3
```
Тело ответа:
```json
{
  "content": [
    {
      "id": 1,
      "time": "2021-01-24T20:33:50.214+00:00",
      "delivered": true,
      "commentEntity": {
        "id": 1,
        "commentId": 1,
        "time": "2021-01-24T20:33:50.214+00:00",
        "delivered": true
      }
    },
    {
      "id": 2,
      "time": "2021-01-24T20:33:52.688+00:00",
      "delivered": true,
      "commentEntity": {
        "id": 2,
        "commentId": 2,
        "time": "2021-01-24T20:33:52.688+00:00",
        "delivered": true
      }
    },
    {
      "id": 3,
      "time": "2021-01-24T20:33:54.201+00:00",
      "delivered": true,
      "commentEntity": {
        "id": 3,
        "commentId": 3,
        "time": "2021-01-24T20:33:54.201+00:00",
        "delivered": true
      }
    }
  ],
  "pageable": {
    "sort": {
      "sorted": true,
      "unsorted": false,
      "empty": false
    },
    "pageNumber": 0,
    "pageSize": 3,
    "offset": 0,
    "paged": true,
    "unpaged": false
  },
  "last": false,
  "totalPages": 2,
  "totalElements": 6,
  "sort": {
    "sorted": true,
    "unsorted": false,
    "empty": false
  },
  "first": true,
  "number": 0,
  "numberOfElements": 3,
  "size": 3,
  "empty": false
}
```
